import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Poste } from '../models/Poste.model';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { Division } from '../models/Division.model';
import { DivisionService } from '../services/Division.service';
import { FilialeService } from '../services/Filiale.service';
import { DirectionService } from '../services/Direction.service';
import { Filiale } from '../models/Filiale.model';
import { Direction } from '../models/Direction.model';
import { Service } from '../models/Service.model';
import { ServiceService } from '../services/Service.service';
import { PosteService } from '../services/Poste.service';
import { Position } from '../models/Position.model';

@Component({
  selector: 'app-new-poste',
  templateUrl: './new-poste.component.html',
  styleUrls: ['./new-poste.component.scss']
})
export class NewPosteComponent implements OnInit {
  posteForm:FormGroup;
  divisions: Division[];
  divisionSubscription: Subscription;
  filiales: Filiale[];
  filialeSubscription: Subscription;
  directions: Direction[];
  directionSubscription: Subscription;
  services: Service[];
  serviceSubscription: Subscription;

  postes: Poste[];
  posteSubscription: Subscription;
  posteexistante:boolean=false;
  niv:number=1;

  
  constructor(private formBuilder: FormBuilder,private divisionService: DivisionService, 
    private router:Router,private filialeService: FilialeService,
    private directionService :DirectionService,private serviceService: ServiceService,
    private posteService : PosteService ){ }

  ngOnInit(): void {

   this.serviceSubscription = this.serviceService.serviceSubject.subscribe(
      (services: Service[]) => {
        this.services = services;
      }
    );
    this.serviceService.emitServices();

     this.directionSubscription = this.directionService.directionSubject.subscribe(
        (directions: Direction[]) => {
          this.directions = directions;
        }
      );
      this.directionService.emitDirections();

 this.divisionSubscription = this.divisionService.divisionSubject.subscribe(
    (divisions: Division[]) => {
      this.divisions = divisions;
    }
  );
  this.divisionService.emitDivisions();

    this.filialeSubscription = this.filialeService.filialeSubject.subscribe(
      (filiales: Filiale[]) => {
        this.filiales = filiales;
      }
    );
    this.filialeService.emitFiliales();

    this.posteForm = this.formBuilder.group({
      intituleposte: ['' ],
      categorieposte: [ ''],
      division: 'aucune',
      filiale: 'aucune',
      direction: 'aucune',
      service: 'aucune'
  
    });

  }


  newPoste:Poste;
  onSubmitForm(){
     const formValue = this.posteForm.value;
    
     if(this.posteService.existePoste(formValue['intituleposte'] )){
      this.posteexistante=true;
    }
    else {
      PosteService.id++;
      this.newPoste = new Poste('poste'+PosteService.id,formValue['intituleposte'],formValue['categorieposte'] ,
      this.posteForm.get('division').value ,this.posteForm.get('filiale').value , this.posteForm.get('direction').value ,
      this.posteForm.get('service').value 
       );
     

    this.posteService.addPoste(this.newPoste);
    this.posteService.emitPostes();
   
    }
    this.router.navigate(['/postes']);
  }
 
  ngOnDestroy() {
    this.filialeSubscription.unsubscribe();
  }
}
