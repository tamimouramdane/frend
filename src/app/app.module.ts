import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DivisionListComponent } from './division-list/division-list.component';
import { FilialeListComponent } from './filiale-list/filiale-list.component';
import { DirectionListComponent } from './direction-list/direction-list.component';
import { EmployeListComponent } from './employe-list/employe-list.component';
import { DirectionService } from './services/Direction.service';
import { DivisionService } from './services/Division.service';
import { EmployeService } from './services/Employe.service';
import { FilialeService } from './services/Filiale.service';
import { PosteService } from './services/Poste.service';
import { ServiceService } from './services/Service.service';
import { NewDivisionComponent } from './new-division/new-division.component';
import { AuthService } from './services/auth.service';
import { AuthComponent } from './auth/auth.component';
import { NewFilialeComponent } from './new-filiale/new-filiale.component';
import { NewDirectionComponent } from './new-direction/new-direction.component';
import { ServiceListComponent } from './service-list/service-list.component';
import { NewServiceComponent } from './new-service/new-service.component';
import { PosteListComponent } from './poste-list/poste-list.component';
import { NewPosteComponent } from './new-poste/new-poste.component';
import { NewEmployeComponent } from './new-employe/new-employe.component';
import { ObjectifListComponent } from './objectif-list/objectif-lis.component';
import { ObjectifService } from './services/Objectif.service';
import { ModEmployeComponent } from './mod-employe/mod-employe.component';
import { MainNavComponent } from './main-nav/main-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Responsable1Component } from './responsable1/responsable1.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserService } from './services/user.service';
import { HttpClientModule } from '@angular/common/http';

const appRoutes: Routes = [
  { path: 'divisions', component: DivisionListComponent },
  { path: 'new-division', component: NewDivisionComponent },
  { path: 'filiales', component: FilialeListComponent },
  { path: 'new-filiale', component: NewFilialeComponent },
  { path: 'directions', component: DirectionListComponent },
  { path: 'new-direction', component: NewDirectionComponent },
  { path: 'services', component: ServiceListComponent },
  { path: 'new-service', component: NewServiceComponent },
  { path: 'postes', component: PosteListComponent },
  { path: 'new-poste', component: NewPosteComponent },
  { path: 'employes', component: EmployeListComponent },
  { path: 'new-employe', component: NewEmployeComponent },
  { path: 'employe/:CodeEmploye', component: ModEmployeComponent },
  { path: 'objectifs', component:ObjectifListComponent},
  { path: 'responsable1' , component:Responsable1Component},
  { path: 'users', component: UserListComponent },
  { path: 'auth', component:AuthComponent },
  { path: '', component: AuthComponent }

];


@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    DivisionListComponent,
    FilialeListComponent,
    DirectionListComponent,
    EmployeListComponent,
    NewDivisionComponent,
    NewFilialeComponent,
    NewDirectionComponent,
    ServiceListComponent,
    NewServiceComponent,
    PosteListComponent,
    NewPosteComponent,
    NewEmployeComponent,
    ObjectifListComponent,
    ModEmployeComponent,
    MainNavComponent,
    Responsable1Component,
    UserListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes),
    LayoutModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule
  ],
  exports: [RouterModule],
  providers: [
    DivisionService,
    DirectionService,
    EmployeService,
    FilialeService,
    PosteService,
    ServiceService,
    ObjectifService,
    AuthService,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
