import { Component, OnInit, OnDestroy } from '@angular/core';
import { Filiale } from '../models/Filiale.model';
import { Subscription } from 'rxjs';
import { FilialeService } from '../services/Filiale.service';
import { Router } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { DirectionService } from '../services/Direction.service';

@Component({
  selector: 'app-filiale-list',
  templateUrl: './filiale-list.component.html',
  styleUrls: ['./filiale-list.component.scss']
})
export class FilialeListComponent implements OnInit ,OnDestroy{

  filiales: Filiale[];
  filialeSubscription: Subscription;
  listForm: FormGroup;
  constructor(private filialeService: FilialeService, private router:Router,private directionService:DirectionService) { }

  ngOnInit() {
    this.filialeSubscription = this.filialeService.filialeSubject.subscribe(
      (filiales: Filiale[]) => {
        this.filiales = filiales;
      }
    );
    this.filialeService.emitFiliales();
  }
  onNouvelleFiliale(){
    this.router.navigate(['new-filiale']);
  }

  onSupprimerFiliale(filiale:Filiale){
   
  this.filialeService.supFiliale(filiale);
  }

  onModifierFiliale(filiale:Filiale){

  }

  ngOnDestroy() {
    this.filialeSubscription.unsubscribe();
  }

}