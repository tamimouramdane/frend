






import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { DirectionService } from '../services/Direction.service';
import { Router } from '@angular/router';
import { Direction } from '../models/Direction.model';
import { Filiale } from '../models/Filiale.model';
import { Subscription } from 'rxjs';
import { FilialeService } from '../services/Filiale.service';

@Component({
  selector: 'app-new-direction',
  templateUrl: './new-direction.component.html',
  styleUrls: ['./new-direction.component.scss']
})
export class NewDirectionComponent implements OnInit, OnDestroy {
  directionForm:FormGroup;
  filiales: Filiale[];
  filialeSubscription: Subscription;
  directionSubscription: Subscription;
  directions:Direction[];
  directionexistante:boolean=false;

  constructor(private formBuilder: FormBuilder,private directionService: DirectionService, 
    private router: Router,private filialeService: FilialeService) { }

  ngOnInit(): void {
    this.filialeSubscription = this.filialeService.filialeSubject.subscribe(
      (filiales: Filiale[]) => {
        this.filiales = filiales;
      }
    );
    this.filialeService.emitFiliales();

    this.directionSubscription = this.directionService.directionSubject.subscribe(
      (directions: Direction[]) => {
        this.directions = directions;
      }
    );
    this.directionService.emitDirections();


    this.directionForm = this.formBuilder.group({
      codedirection: [''],
      intituledirection: ['' ],
      intitulemere: 'aucune'
    });

  }

  newDirection:Direction;
  onSubmitForm(){
    const formValue = this.directionForm.value;

    if(this.directionService.existeDirection(formValue['intituledirection'] )){
      this.directionexistante=true;
    }
    else {
    DirectionService.id++;
    
    if(this.directionForm.get('intitulemere').value=='aucune'){
      this.newDirection = new Direction('direction'+DirectionService.id,'direction',formValue['codedirection'], formValue['intituledirection'] );

    }else{
     this.newDirection = new Direction('direction'+DirectionService.id,'direction',formValue['codedirection'],formValue['intituledirection'],
    this.directionForm.get('intitulemere').value );
    this.filialeService.addDirectionFiliale(this.newDirection,this.directionForm.get('intitulemere').value);
    
    }
  
    
    this.directionService.addDirection(this.newDirection);
    this.directionService.emitDirections();
    
    this.router.navigate(['/directions']);
    }
  }

  ngOnDestroy() {
    this.filialeSubscription.unsubscribe();
  }
}






