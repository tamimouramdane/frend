import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewFilialeComponent } from './new-filiale.component';

describe('NewFilialeComponent', () => {
  let component: NewFilialeComponent;
  let fixture: ComponentFixture<NewFilialeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewFilialeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewFilialeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
