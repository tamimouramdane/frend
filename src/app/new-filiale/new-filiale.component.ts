import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { FilialeService } from '../services/Filiale.service';
import { Router } from '@angular/router';
import { Filiale } from '../models/Filiale.model';
import { Division } from '../models/Division.model';
import { Subscription } from 'rxjs';
import { DivisionService } from '../services/Division.service';

@Component({
  selector: 'app-new-filiale',
  templateUrl: './new-filiale.component.html',
  styleUrls: ['./new-filiale.component.scss']
})
export class NewFilialeComponent implements OnInit, OnDestroy {
  filialeForm:FormGroup;
  divisions: Division[];
  divisionSubscription: Subscription;
  filialeSubscription: Subscription;
  filiales:Filiale[];
  filialeexistante:boolean=false;

  constructor(private formBuilder: FormBuilder,private filialeService: FilialeService, 
    private router: Router,private divisionService: DivisionService) { }

  ngOnInit(): void {
    this.divisionSubscription = this.divisionService.divisionSubject.subscribe(
      (divisions: Division[]) => {
        this.divisions = divisions;
      }
    );
    this.divisionService.emitDivisions();

    this.filialeSubscription = this.filialeService.filialeSubject.subscribe(
      (filiales: Filiale[]) => {
        this.filiales = filiales;
      }
    );
    this.filialeService.emitFiliales();


    this.filialeForm = this.formBuilder.group({
      codefiliale: [''],
      intitulefiliale: ['' ],
      intitulemere: 'aucune'
    });

  }

  newFiliale:Filiale;
  onSubmitForm(){
    const formValue = this.filialeForm.value;

    if(this.filialeService.existeFiliale(formValue['intitulefiliale'] )){
      this.filialeexistante=true;
    }
    else {
    FilialeService.id++;
    
    if(this.filialeForm.get('intitulemere').value=='aucune'){
      this.newFiliale = new Filiale('filiale'+FilialeService.id,'filiale',formValue['codefiliale'],formValue['intitulefiliale'] );

    }else{
     this.newFiliale = new Filiale('filiale'+FilialeService.id,'filiale',formValue['codefiliale'],formValue['intitulefiliale'],
    this.filialeForm.get('intitulemere').value );
    this.divisionService.addFilialeDivision(this.newFiliale,this.filialeForm.get('intitulemere').value);
    
    }
  
    
    this.filialeService.addFiliale(this.newFiliale);
    this.filialeService.emitFiliales();
    
    this.router.navigate(['/filiales']);
    }
  }

  ngOnDestroy() {
    this.divisionSubscription.unsubscribe();
  }
}






