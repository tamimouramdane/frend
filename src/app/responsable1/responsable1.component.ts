import { Component, OnInit } from '@angular/core';
import { EmployeService } from '../services/Employe.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Employe } from '../models/Employe.model';


@Component({
  selector: 'app-responsable1',
  templateUrl: './responsable1.component.html',
  styleUrls: ['./responsable1.component.scss']
})
export class Responsable1Component implements OnInit {
  employes: Employe[];
  employeSubscription: Subscription;
  employe:Employe;
  constructor(private employeService:EmployeService, private router:Router) { }

  ngOnInit() {
    this.employeSubscription = this.employeService.employeSubject.subscribe(
      (employes: Employe[]) => {
        this.employes = employes;
      }
    );
    this.employeService.emitEmployes();
  }
  

 

}
