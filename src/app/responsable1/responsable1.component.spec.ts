import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Responsable1Component } from './responsable1.component';

describe('Responsable1Component', () => {
  let component: Responsable1Component;
  let fixture: ComponentFixture<Responsable1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Responsable1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Responsable1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
