import { Position } from './Position.model';
import { Service } from './Service.model';
import { Filiale } from './Filiale.model';

export class Direction extends Position {
    
    
    constructor(
        public  CodePosition:string,
        public IntitulePosition: string,
        public CodeDirection:string,
        public IntituleDirection:string,
        public FilialeMere ?:Filiale,
        public Services?:Service[]
      ) {
        super(CodePosition,IntitulePosition);
      }
  }