
export class Objectif {
    constructor(
      public  CodeObjectif:string,
      public IntituleObjectif: string,
      public Ponderation: string,
      public PlanAction :string,
      public Kpi : string,
      public Cible : string,
      public Evaluation ? : string
    ) {}
  }