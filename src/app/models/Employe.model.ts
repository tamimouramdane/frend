import { Poste } from './Poste.model';
import { User } from './User.model';
export class Employe {
    constructor(
      public  CodeEmploye:string,
      public Nom: string,
      public Prenom: string,
      public Poste:string,
      public user:User,
      public Responsable ?:Employe
        /*
      public Email : string,
      public Password:string,
      public Collaborateurs ?:Employe[]
      */
    ) {}
  }