import { Filiale } from './Filiale.model';

export class Poste {
    constructor(
      public  CodePoste:string,
      public IntitulePoste: string,
      public Categorie: string,
      public Division ? :string,
      public Filiale ?: string,
      public Direction ?: string,
      public Service ? : string
    ) {}
  }