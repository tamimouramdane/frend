import { Position } from './Position.model';
import { Direction } from './Direction.model';


export class Service extends Position {
    
    
    constructor(
        public  CodePosition:string,
        public IntitulePosition: string,
        public CodeService :string,
        public IntituleService:string,
        public DirectionMere  :Direction
        
      ) {
        super(CodePosition,IntitulePosition);
      }
  }