import { Position } from './Position.model';
import { Direction } from './Direction.model';
import { Division } from './Division.model';

export class Filiale extends Position {
    
    
    constructor(
        public  CodePosition:string,
        public IntitulePosition: string,
        public CodeFiliale:string,
        public IntituleFiliale:string,
        public DivisionMere ? :Division,
        public Directions ? :Direction[]

       
      ) {
        super(CodePosition,IntitulePosition);
      }
  }