import { Role } from './Role.model';

export class User {
  /*
    id: number;
    username:string;
    password:string;
    email: string;
    role:number;
*/
    
    constructor(
      public id: number,
      public userName  :string,
      public email : string,
      public password:string,
      public role:number
      
    ) {}
    
  }