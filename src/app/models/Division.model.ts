import { Position } from './Position.model';
import { Filiale } from './Filiale.model';
import { Objectif } from './Objectif.model';

export class Division extends Position {
    
    
    constructor(
        public  CodePosition:string,
        public IntitulePosition: string,
        public CodeDivision:string,
        public IntituleDivision:string,
        public Filiales ? :Filiale[],
        public ObjectifDivision ? :Objectif
      ) {
        super(CodePosition,IntitulePosition);
      }

      SetIntituleDivision(intituledivision:string){
        this.IntituleDivision=intituledivision;
                  }
  }