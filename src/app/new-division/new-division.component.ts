
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DivisionService } from '../services/Division.service';
import { Router } from '@angular/router';
import { Division } from '../models/Division.model';


@Component({
  selector: 'app-new-division',
  templateUrl: './new-division.component.html',
  styleUrls: ['./new-division.component.scss']
})

export class NewDivisionComponent implements OnInit {

  divisionForm: FormGroup;
  divisionexistante:boolean=false;
  constructor(private formBuilder: FormBuilder,
            private divisionService: DivisionService, 
              private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.divisionForm = this.formBuilder.group({
      codedivision:[''],
      intituledivision: ['' ]
    });
  }
  
  onSubmitForm() {
    
    const formValue = this.divisionForm.value;

    if(this.divisionService.existeDivision(formValue['intituledivision'] )){
      this.divisionexistante=true;
    }
    else {
    DivisionService.id++;
    const newDivision = new Division('division'+DivisionService.id,'division',formValue['codedivision'],formValue['intituledivision']);
    this.divisionService.addDivision(newDivision);
    this.divisionService.emitDivisions();
    
    this.router.navigate(['/divisions']);
    }
  }

 

}