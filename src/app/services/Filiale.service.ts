import { Filiale } from '../models/Filiale.model';
import { Subject } from 'rxjs/Subject';
import { Direction } from '../models/Direction.model';
import { DirectionService } from './Direction.service';
import { Injectable } from '@angular/core';

@Injectable()
export class FilialeService {
  static id:number=0;
  private filiales: Filiale[]= [
   
];
  filialeSubject = new Subject<Filiale[]>();

  constructor(private directionService: DirectionService) { }

  emitFiliales() {
    this.filialeSubject.next(this.filiales.slice());
  }
   
  addFiliale(filiale: Filiale) {
    this.filiales.push(filiale);
    this.emitFiliales();
  }
   
  supFiliale(filiale: Filiale) {
    this.filiales.splice( this.filiales.indexOf(filiale),1);
     this.emitFiliales();
   }
   
  existeFiliale(intitulefiliale:string):boolean{
    for(let filiale of this.filiales){
      if(filiale.IntituleFiliale===intitulefiliale){
        return true;
      }
    }
    return false;
  }

  cpt:number=0;
  addDirectionFiliale(direction:Direction,filiale:Filiale){
   this.cpt=this.filiales.indexOf(filiale);

    if(this.filiales[this.cpt].Directions!=null){
    this.filiales[this.cpt].Directions.push(direction);
    }
    else{
      this.filiales[this.cpt].Directions=[direction];
    }
  }

}