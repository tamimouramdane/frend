import { User } from '../models/User.model';
import { Subject } from 'rxjs/Subject';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class UserService {
  private users: User[]=[ new User(1,'admins','admins','admins@gmail.com',0)];
  userSubject = new Subject<User[]>();

  constructor(private httpClient: HttpClient) { }

  emitUsers() {
    this.userSubject.next(this.users.slice());
  }

  addUser(user: User) {
    this.users.push(user);
    this.emitUsers();
  }

  saveUsersToServer() {
    this.httpClient
      .put('https://http-client-demo-744c9.firebaseio.com/users.json', this.users)
      .subscribe(
        () => {
          console.log('Enregistrement terminé !');
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      );
}

getUsersFromServer() {
  this.httpClient
    .get<any[]>('https://http-client-demo-744c9.firebaseio.com/users.json')
    .subscribe(
      (response) => {
        this.users = response;
        this.emitUsers();
      },
      (error) => {
        console.log('Erreur ! : ' + error);
      }
    );
}

}