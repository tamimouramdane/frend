import { Objectif } from '../models/Objectif.model';
import { Subject } from 'rxjs/Subject';
import { Injectable } from '@angular/core';



@Injectable()
export class ObjectifService {
    static id:number=0;
    objectifs: Objectif[]= [new Objectif('groupe','a','a','a','a','a')];
  objectifSubject = new Subject<Objectif[]>();
  
  constructor() { }

  emitObjectifs() {
    this.objectifSubject.next(this.objectifs.slice());
  }

  addObjectif(objectif: Objectif) {
    this.objectifs.push(objectif);
    this.emitObjectifs();
  }
  
  supObjectif(objectif: Objectif) {
   this.objectifs.splice( this.objectifs.indexOf(objectif),1);
    this.emitObjectifs();
  }

  existeObjectif(codeobjectif:string):boolean{
    for(let objectif of this.objectifs){
      if(objectif.CodeObjectif===codeobjectif){
        return true;
      }
    }
    return false;
  }
  cpt:number=0;
  getObjectif(codeobjectif:string):Objectif{
    for(let objectif of this.objectifs){
      if(objectif.CodeObjectif===codeobjectif){
        this.cpt=this.objectifs.indexOf(objectif);
        return this.objectifs[this.cpt];
      }
    }
  }

  modifierObjectif(objectif:Objectif){
   this.objectifs[this.objectifs.indexOf(objectif)]=objectif;
  }
}