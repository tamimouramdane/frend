import { Service } from '../models/Service.model';
import { Subject } from 'rxjs/Subject';

import { Injectable } from '@angular/core';

@Injectable()
export class ServiceService {
  static id:number=0;
  private services: Service[]=[];
  serviceSubject = new Subject<Service[]>();
  
  emitServices() {
    this.serviceSubject.next(this.services.slice());
  }

  addService(service: Service) {
    this.services.push(service);
    this.emitServices();
  }

  
  supService(service: Service) {
    this.services.splice( this.services.indexOf(service),1);
     this.emitServices();
   }
   
  existeService(intituleservice:string):boolean{
    for(let service of this.services){
      if(service.IntituleService===intituleservice){
        return true;
      }
    }
    return false;
  }

  
}