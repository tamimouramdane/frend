import { Direction } from '../models/Direction.model';
import { Subject } from 'rxjs/Subject';
import { Service } from '../models/Service.model';
import { ServiceService } from './Service.service';
import { Injectable } from '@angular/core';

@Injectable()
export class DirectionService {
  static id:number=0;
  private directions: Direction[]= [];
  directionSubject = new Subject<Direction[]>();

  constructor(private serviceService: ServiceService) { }

  emitDirections() {
    this.directionSubject.next(this.directions.slice());
  }
   
  addDirection(direction: Direction) {
    this.directions.push(direction);
    this.emitDirections();
  }
   
  supDirection(direction: Direction) {
    this.directions.splice( this.directions.indexOf(direction),1);
     this.emitDirections();
   }
 
  existeDirection(intituledirection:string):boolean{
    for(let direction of this.directions){
      if(direction.IntituleDirection===intituledirection){
        return true;
      }
    }
    return false;
  }

  cpt:number=0;
   addServiceDirection(service:Service,direction:Direction){
    this.cpt=this.directions.indexOf(direction);
 
     if(this.directions[this.cpt].Services!=null){
     this.directions[this.cpt].Services.push(service);
     }
     else{
       this.directions[this.cpt].Services=[service];
     }
   }

  
}