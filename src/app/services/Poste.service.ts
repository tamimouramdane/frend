import { Poste } from '../models/Poste.model';
import { Subject } from 'rxjs/Subject';

export class PosteService {
  static id:number=0;
  private postes: Poste[]=[];
  posteSubject = new Subject<Poste[]>();

  emitPostes() {
    this.posteSubject.next(this.postes.slice());
  }

  addPoste(poste: Poste) {
    this.postes.push(poste);
    this.emitPostes();
  }

   
  supPoste(poste: Poste) {
    this.postes.splice( this.postes.indexOf(poste),1);
     this.emitPostes();
   }
   
  existePoste(intituleposte:string):boolean{
    for(let poste of this.postes){
      if(poste.IntitulePoste===intituleposte){
        return true;
      }
    }
    return false;
  }
}