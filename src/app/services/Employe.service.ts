import { Employe } from '../models/Employe.model';
import { Subject } from 'rxjs/Subject';

export class EmployeService {
  static id:number=0;
  private employes: Employe[]= [];
  employeSubject = new Subject<Employe[]>();

  emitEmployes() {
    this.employeSubject.next(this.employes.slice());
  }

  addEmploye(employe: Employe) {
    this.employes.push(employe);
    this.emitEmployes();
  }

  supEmploye(employe: Employe) {
    this.employes.splice( this.employes.indexOf(employe),1);
     this.emitEmployes();
   }

   existeEmploye(CodeEmploye:string):boolean{
    for(let employe of this.employes){
      if(employe.CodeEmploye===CodeEmploye){
        return true;
      }
    }
    return false;
  }
  cpt:number=0;
  getEmploye(CodeEmploye:string):Employe{
    for(let employe of this.employes){
      if(employe.CodeEmploye===CodeEmploye){
        this.cpt=this.employes.indexOf(employe);
        return this.employes[this.cpt];
      }
    }
  }
}