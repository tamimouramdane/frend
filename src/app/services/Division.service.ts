import { Division } from '../models/Division.model';
import { Subject } from 'rxjs/Subject';
import { Injectable } from '@angular/core';
import { Objectif } from '../models/Objectif.model';
import { Filiale } from '../models/Filiale.model';

@Injectable()
export class DivisionService {
    static id:number=0;
    divisions: Division[]= [];
  divisionSubject = new Subject<Division[]>();
  
  constructor() { }

  emitDivisions() {
    this.divisionSubject.next(this.divisions.slice());
  }

  addDivision(division: Division) {
    this.divisions.push(division);
    this.emitDivisions();
  }
  
  supDivision(division: Division) {
   this.divisions.splice( this.divisions.indexOf(division),1);
    this.emitDivisions();
  }

  existeDivision(intituledivision:string):boolean{
    for(let division of this.divisions){
      if(division.IntituleDivision===intituledivision){
        return true;
      }
    }
    return false;
  }
  cpt:number=0;
  getDivision(intituledivision:string):Division{
    for(let division of this.divisions){
      if(division.IntituleDivision===intituledivision){
        this.cpt=this.divisions.indexOf(division);
        return this.divisions[this.cpt];
      }
    }
  }

  modifierDivisionObjectif(division:Division,objectif :Objectif){
    this.divisions[this.divisions.indexOf(division)].ObjectifDivision=objectif;
   }


   addFilialeDivision(filiale:Filiale,division:Division){
    this.cpt=this.divisions.indexOf(division);
 
     if(this.divisions[this.cpt].Filiales!=null){
     this.divisions[this.cpt].Filiales.push(filiale);
     }
     else{
       this.divisions[this.cpt].Filiales=[filiale];
     }
   }
}