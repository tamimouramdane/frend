import { Component, OnInit, OnDestroy } from '@angular/core';
import { Direction } from '../models/Direction.model';
import { Subscription } from 'rxjs';
import { DirectionService } from '../services/Direction.service';
import { Router } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { ServiceService } from '../services/Service.service';

@Component({
  selector: 'app-direction-list',
  templateUrl: './direction-list.component.html',
  styleUrls: ['./direction-list.component.scss']
})
export class DirectionListComponent implements OnInit ,OnDestroy{

  directions: Direction[];
  directionSubscription: Subscription;
  listForm: FormGroup;
  constructor(private directionService: DirectionService, private router:Router,private serviceService:ServiceService) { }

  ngOnInit() {
    this.directionSubscription = this.directionService.directionSubject.subscribe(
      (directions: Direction[]) => {
        this.directions = directions;
      }
    );
    this.directionService.emitDirections();
  }
  onNouvelleDirection(){
    this.router.navigate(['new-direction']);
  }

  onSupprimerDirection(direction:Direction){
   
  this.directionService.supDirection(direction);
  }

  onModifierDirection(direction:Direction){

  }


  ngOnDestroy() {
    this.directionSubscription.unsubscribe();
  }

}