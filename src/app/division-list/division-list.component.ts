import { Component, OnInit,OnDestroy } from '@angular/core';
import { Division } from '../models/Division.model';
import { Subscription } from 'rxjs';
import { DivisionService } from '../services/Division.service';
import { Router } from '@angular/router';
import { FilialeService } from '../services/Filiale.service';
import { DirectionService } from '../services/Direction.service';

@Component({
  selector: 'app-division-list',
  templateUrl: './division-list.component.html',
  styleUrls: ['./division-list.component.scss']
})
export class DivisionListComponent implements OnInit, OnDestroy {

  divisions: Division[];
  divisionSubscription: Subscription;

  constructor(private divisionService: DivisionService, private router:Router,private filialeService: FilialeService,
              private directionService :DirectionService) { }

  ngOnInit() {
    this.divisionSubscription = this.divisionService.divisionSubject.subscribe(
      (divisions: Division[]) => {
        this.divisions = divisions;
      }
    );
    this.divisionService.emitDivisions();
  }

  onNouvelleDivision(){
    
    this.router.navigate(['new-division']);
  }

  onSupprimerDivision(division:Division){
    
  this.divisionService.supDivision(division);
  }

  onModifierDivision(division:Division){

  }

  ngOnDestroy() {
    this.divisionSubscription.unsubscribe();
  }


}