import { Component, OnInit, OnDestroy } from '@angular/core';
import { Employe } from '../models/Employe.model';
import { Subscription } from 'rxjs';
import { EmployeService } from '../services/Employe.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-employe-list',
  templateUrl: './employe-list.component.html',
  styleUrls: ['./employe-list.component.scss']
})
export class EmployeListComponent implements OnInit ,OnDestroy{

  employes: Employe[];
  employeSubscription: Subscription;
  constructor(private employeService: EmployeService,private router:Router) { }

  ngOnInit() {
    this.employeSubscription = this.employeService.employeSubject.subscribe(
      (employes: Employe[]) => {
        this.employes = employes;
      }
    );
    this.employeService.emitEmployes();
  }

  onModemploye(employe:Employe){
    this.router.navigate(['employe/:'+employe.CodeEmploye]);
  }
  onNouveauEmploye(){
    
    this.router.navigate(['new-employe']);
  }
  
  onSupprimerEmploye(employe:Employe){
  this.employeService.supEmploye(employe);
  }

  ngOnDestroy() {
    this.employeSubscription.unsubscribe();
  }

}