






import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { ServiceService } from '../services/Service.service';
import { Router } from '@angular/router';
import { Service } from '../models/Service.model';
import { Direction } from '../models/Direction.model';
import { Subscription } from 'rxjs';
import { DirectionService } from '../services/Direction.service';

@Component({
  selector: 'app-new-service',
  templateUrl: './new-service.component.html',
  styleUrls: ['./new-service.component.scss']
})
export class NewServiceComponent implements OnInit, OnDestroy {
  serviceForm:FormGroup;
  directions: Direction[];
  directionSubscription: Subscription;
  serviceSubscription: Subscription;
  services:Service[];
  serviceexistante:boolean=false;

  constructor(private formBuilder: FormBuilder,private serviceService: ServiceService, 
    private router: Router,private directionService: DirectionService) { }

  ngOnInit(): void {
    this.directionSubscription = this.directionService.directionSubject.subscribe(
      (directions: Direction[]) => {
        this.directions = directions;
      }
    );
    this.directionService.emitDirections();

    this.serviceSubscription = this.serviceService.serviceSubject.subscribe(
      (services: Service[]) => {
        this.services = services;
      }
    );
    this.serviceService.emitServices();


    this.serviceForm = this.formBuilder.group({
      codeservice:[''],
      intituleservice: ['' ],
      intitulemere: 'aucune'
    });

  }

  newService:Service;
  onSubmitForm(){
    const formValue = this.serviceForm.value;

    if(this.serviceService.existeService(formValue['intituleservice'] )){
      this.serviceexistante=true;
    }
    else {
    ServiceService.id++;
    
   
     this.newService = new Service('service'+ServiceService.id,'service',formValue['codeservice'], formValue['intituleservice'],
    this.serviceForm.get('intitulemere').value );
    this.directionService.addServiceDirection(this.newService,this.serviceForm.get('intitulemere').value);
    
    
  
    
    this.serviceService.addService(this.newService);
    this.serviceService.emitServices();
    
    this.router.navigate(['/services']);
    }
  }

  ngOnDestroy() {
    this.directionSubscription.unsubscribe();
  }
}






