import { Component, OnInit, OnDestroy } from '@angular/core';
import { Service } from '../models/Service.model';
import { Subscription } from 'rxjs';
import { ServiceService } from '../services/Service.service';
import { Router } from '@angular/router';
import { FormGroup } from '@angular/forms';


@Component({
  selector: 'app-service-list',
  templateUrl: './service-list.component.html',
  styleUrls: ['./service-list.component.scss']
})
export class ServiceListComponent implements OnInit ,OnDestroy{

  services: Service[];
  serviceSubscription: Subscription;
  listForm: FormGroup;
  constructor(private serviceService: ServiceService, private router:Router) { }

  ngOnInit() {
    this.serviceSubscription = this.serviceService.serviceSubject.subscribe(
      (services: Service[]) => {
        this.services = services;
      }
    );
    this.serviceService.emitServices();
  }
  onNouvelleService(){
    this.router.navigate(['new-service']);
  }

  onSupprimerService(service:Service){

  this.serviceService.supService(service);
  }

  onModifierService(service:Service){
    
  }


  ngOnDestroy() {
    this.serviceSubscription.unsubscribe();
  }

}