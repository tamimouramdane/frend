import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Division } from '../models/Division.model';
import { DivisionService } from '../services/Division.service';
import { Filiale } from '../models/Filiale.model';
import { FilialeService } from '../services/Filiale.service';
import { Objectif } from '../models/Objectif.model';
import { ObjectifService } from '../services/Objectif.service';

@Component({
  selector: 'app-objectif-list',
  templateUrl: './objectif-list.component.html',
  styleUrls: ['./objectif-list.component.scss']
})
export class ObjectifListComponent implements OnInit {
  divisions: Division[];
  divisionSubscription: Subscription;
  filiales: Filiale[];
  filialeSubscription: Subscription;
  objectifs: Objectif[];
  objectifSubscription: Subscription;
  objectifgroupe:Objectif;
  div:string;
  val:string;
 
  constructor(private divisionService: DivisionService,private filialeService: FilialeService,private objectifService: ObjectifService) { }

  ngOnInit(): void {
    this.divisionSubscription = this.divisionService.divisionSubject.subscribe(
      (divisions: Division[]) => {
        this.divisions = divisions;
      }
    );
    this.divisionService.emitDivisions();

    this.filialeSubscription = this.filialeService.filialeSubject.subscribe(
      (filiales: Filiale[]) => {
        this.filiales = filiales;
      }
    );
    this.filialeService.emitFiliales();

    this.objectifSubscription = this.objectifService.objectifSubject.subscribe(
      (objectifs: Objectif[]) => {
        this.objectifs = objectifs;
      }
    );
    this.objectifService.emitObjectifs();
   /*
    for(let objectif of this.objectifs){
      if(objectif.CodeObjectif='groupe'){
        
      }
      else{
        if(this.divisionService.existeDivision(objectif.CodeObjectif)){
          this.divisionService.getDivision(objectif.CodeObjectif);
        }
        else{

        }
      }
    }

    this.objectifgroupe=this.objectifService.getObjectif('groupe');
     
   this.val=(<HTMLInputElement>document.getElementById("pondération")).value;
   
  console.log((<HTMLInputElement>document.getElementById("obj")).value);
  */
  }
  
  onEnregistrerDiv(division:Division){
   console.log((<HTMLInputElement>document.getElementById("objectifdivision"+division.IntituleDivision)).value);
   const objectif =new Objectif(division.IntituleDivision,
     (<HTMLInputElement>document.getElementById("objectifdivision"+division.IntituleDivision)).value,
   (<HTMLInputElement>document.getElementById("pondérationdivision"+division.IntituleDivision)).value,
   (<HTMLInputElement>document.getElementById("plandactiondivision"+division.IntituleDivision)).value,
   (<HTMLInputElement>document.getElementById("kpidivision"+division.IntituleDivision)).value,
   (<HTMLInputElement>document.getElementById("cibledivision"+division.IntituleDivision)).value,
   (<HTMLInputElement>document.getElementById("Evaluationdivision"+division.IntituleDivision)).value);
   
   this.divisionService.modifierDivisionObjectif(division,objectif);
   
  }
  
  
}