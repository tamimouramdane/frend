import { Component, OnInit, OnDestroy } from '@angular/core';
import { Poste } from '../models/Poste.model';
import { Subscription } from 'rxjs';
import { PosteService } from '../services/Poste.service';
import { Router } from '@angular/router';
import { FormGroup } from '@angular/forms';


@Component({
  selector: 'app-poste-list',
  templateUrl: './poste-list.component.html',
  styleUrls: ['./poste-list.component.scss']
})
export class PosteListComponent implements OnInit ,OnDestroy{

  postes: Poste[];
  posteSubscription: Subscription;
  listForm: FormGroup;
  constructor(private posteService: PosteService, private router:Router) { }

  ngOnInit() {
    this.posteSubscription = this.posteService.posteSubject.subscribe(
      (postes: Poste[]) => {
        this.postes = postes;
      }
    );
    this.posteService.emitPostes();
  }
  onNouvellePoste(){
    this.router.navigate(['new-poste']);
  }

  onSupprimerPoste(poste:Poste){

  this.posteService.supPoste(poste);
  }


  ngOnDestroy() {
    this.posteSubscription.unsubscribe();
  }

}